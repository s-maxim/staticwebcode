package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest{


    @Test
    public void decreasecounterOneTest() throws Exception {
        int k= new Increment().decreasecounter(1);
        assertEquals("testing one", 1, k);
    }

    @Test
    public void decreasecounterZeroTest() throws Exception {
        int k= new Increment().decreasecounter(0);
        assertEquals("testing zero", 1, k);
    }

    @Test
    public void decreasecounterElseTest() throws Exception {
        int k= new Increment().decreasecounter(10);
        assertEquals("testing else", 1, k);
    }





}
